# opengl_box

This isn't much of a program, as it doesn't do too much useful. It was more a test of making a distributable build system and source code. To that extent it works perfectly. The source code here can be compiled on all linux platforms that meet the requirements. There will not be a windows port any time soon, and no plans for a mac port either.
Run 'meson --reconfigure build/' if you choose to build from this repo. Meson adds hard coded paths inside various files ninja uses to compile, so this will have to be run regardless of if you get the dist tar or build from the repo.
## How to run
It isn't hard really. Just run 'opengl_box' from your terminal. Takes no additional arguments, but maybe in the future. Also works with dmenu.

##To do
Make this better, somehow
Add a mac port
Add a windows port
